(function() {
  'use strict';

  angular.module('desafio')
      .controller('HomeController', HomeController);

  HomeController.$inject = ['FornecedorService', 'ProdutoService', 'CompradorService'];

  function HomeController(FornecedorService, ProdutoService, CompradorService) {
      var vm = this;

      vm.listarProdutos = function listarProdutos(fornecedor) {
        if (!fornecedor) {
          vm.produtos = [];
        } else {
          ProdutoService.listar(fornecedor).then(function(res) {
            vm.produtos = res.data;
          });
        }
      };

      vm.listarFornecedores = function listarFornecedores() {
        FornecedorService.listar().then(function(res) {
          vm.fornecedores = res.data;
        });
      };

      vm.listarCompradores = function listarCompradores() {
        CompradorService.listar().then(function(res) {
          vm.compradores = res.data;
        });
      };

      function init() {
        vm.listarCompradores();
        vm.listarFornecedores();
      }

      init();

  }

})();